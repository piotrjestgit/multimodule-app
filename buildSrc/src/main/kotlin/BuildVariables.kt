object BuildVariables{
    val compileSdkVersion = 29
    val minSdkVersion = 21
    val targetSdkVersion = 29
}