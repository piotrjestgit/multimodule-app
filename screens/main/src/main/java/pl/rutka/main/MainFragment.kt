package pl.rutka.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.android.synthetic.main.fragment_main.*
import pl.rutka.base.BaseFragment
import javax.inject.Inject

@Module
interface MainFragmentModule {
    @ContributesAndroidInjector(modules = [])
    fun mainFragment(): MainFragment
}

class MainFragment : BaseFragment() {

    @Inject
    lateinit var navigation: MainFragmentNavigation

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        openListScreenBtn.setOnClickListener {
            navigation.openListScreen()
        }
    }
}