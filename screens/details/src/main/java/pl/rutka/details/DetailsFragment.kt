package pl.rutka.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.android.synthetic.main.fragment_details.*
import pl.rutka.base.BaseFragment
import pl.rutka.base.Constants.CAT_FACT_KEY
import pl.rutka.model.CatFact

@Module
interface DetailsFragmentModule {
    @ContributesAndroidInjector(modules = [])
    fun detailsFragment(): DetailsFragment
}

class DetailsFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onResume() {
        super.onResume()

        //FIXME should use DetailsFragmentArgs
        arguments?.let { bundle ->
            val catFact = bundle.getSerializable(CAT_FACT_KEY)

            catFact?.let {
                val id = (it as CatFact)._id
                val text = it.text

                factID.text = id
                factText.text = text

            }

        }
    }
}