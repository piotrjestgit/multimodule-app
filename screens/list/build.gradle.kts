
plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
}

android {
    compileSdkVersion(BuildVariables.compileSdkVersion)

    defaultConfig {
        minSdkVersion(BuildVariables.minSdkVersion)
        targetSdkVersion(BuildVariables.targetSdkVersion)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"))
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":base"))
    implementation(project(":data:repository"))
    implementation(project(":data:model"))

    implementation("androidx.appcompat:appcompat:${DependencyVersions.appCompat}")
    implementation("com.google.android.material:material:${DependencyVersions.materialUi}")
    implementation("androidx.constraintlayout:constraintlayout:${DependencyVersions.constraintLayout}")

    //Dagger
    implementation("com.google.dagger:dagger:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-compiler:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android-support:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-android-processor:${DependencyVersions.dagger}")

    //Room
    implementation("androidx.room:room-runtime:${DependencyVersions.room}")
    kapt("androidx.room:room-compiler:${DependencyVersions.room}")
    implementation("androidx.room:room-ktx:${DependencyVersions.room}")

    //Fragment
    implementation("androidx.fragment:fragment-ktx:${DependencyVersions.fragment_ktx}")

    //RecyclerView
    implementation("androidx.recyclerview:recyclerview:${DependencyVersions.recyclerView}")
}