package pl.rutka.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pl.rutka.model.CatFact
import pl.rutka.repository.Repository
import javax.inject.Inject

class ListFragmentViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {


    val catFacts: LiveData<List<CatFact>> get() = _catFacts
    private val _catFacts: MutableLiveData<List<CatFact>> = MutableLiveData<List<CatFact>>().apply {
        postValue(emptyList())
    }

    val isLoading: LiveData<Boolean> get() = _isLoading
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{
        postValue(false)
    }

    fun loadData() {
        viewModelScope.launch {
            _isLoading.postValue(true)

            val catFacts = repository.getCatFacts()
            _catFacts.postValue(catFacts)

            _isLoading.postValue(false)
        }
    }
}