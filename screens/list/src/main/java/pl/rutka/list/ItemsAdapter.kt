package pl.rutka.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.rutka.model.CatFact

class ItemsAdapter(
    private val openDetails: (CatFact) -> Unit
) : RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    class ViewHolder(val view: ListItemView) : RecyclerView.ViewHolder(view)

    private val catFacts: MutableList<CatFact> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false) as ListItemView
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val catFact = catFacts[position]
        holder.view.setItem(catFact)
        holder.view.setOnClickListener {
            openDetails(catFact)
        }
    }

    override fun getItemCount(): Int {
        return catFacts.size
    }

    fun submitList(catFacts: List<CatFact>) {
        this.catFacts.clear()
        this.catFacts.addAll(catFacts)
        notifyDataSetChanged()
    }


}


