package pl.rutka.list

import pl.rutka.model.CatFact

interface ListFragmentNavigation {
    fun openCatFactDetails(catFact: CatFact)
}