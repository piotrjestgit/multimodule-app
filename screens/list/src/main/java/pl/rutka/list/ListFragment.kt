package pl.rutka.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.android.synthetic.main.fragment_list.*
import pl.rutka.base.BaseFragment
import pl.rutka.base.ViewModelFactory
import pl.rutka.base.getViewModel
import pl.rutka.model.CatFact
import javax.inject.Inject

@Module
interface ListFragmentModule {
    @ContributesAndroidInjector
    fun listFragment(): ListFragment
}

class ListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<ListFragmentViewModel>

    @Inject
    lateinit var viewModel: ListFragmentViewModel

    @Inject
    lateinit var navigation: ListFragmentNavigation

    private val itemsAdapter by lazy {
        ItemsAdapter {
            openCatFactDetails(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
        recyclerView.adapter = itemsAdapter

        viewModel = viewModelFactory.getViewModel(this)

        viewModel.catFacts.observe(viewLifecycleOwner, Observer {
            //cat facts list is long, so let's take 20 items
            itemsAdapter.submitList(it.take(20))
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            progressBar.isVisible = it
        })
    }

    private fun openCatFactDetails(catFact: CatFact) {
        navigation.openCatFactDetails(catFact)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadData()
    }
}