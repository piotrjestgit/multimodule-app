package pl.rutka.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005J\u0010\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0007H\u0016J\u0006\u0010\f\u001a\u00020\u0007R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lpl/rutka/navigation/Navigator;", "Lpl/rutka/main/MainFragmentNavigation;", "Lpl/rutka/list/ListFragmentNavigation;", "()V", "navController", "Landroidx/navigation/NavController;", "bind", "", "openCatFactDetails", "catFact", "Lpl/rutka/model/CatFact;", "openListScreen", "unbind", "navigation_debug"})
public final class Navigator implements pl.rutka.main.MainFragmentNavigation, pl.rutka.list.ListFragmentNavigation {
    private androidx.navigation.NavController navController;
    
    public final void bind(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController) {
    }
    
    public final void unbind() {
    }
    
    @java.lang.Override()
    public void openListScreen() {
    }
    
    @java.lang.Override()
    public void openCatFactDetails(@org.jetbrains.annotations.NotNull()
    pl.rutka.model.CatFact catFact) {
    }
    
    @javax.inject.Inject()
    public Navigator() {
        super();
    }
}