package pl.rutka.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\t"}, d2 = {"Lpl/rutka/navigation/NavigationModule;", "", "()V", "provideListFragmentNavigation", "Lpl/rutka/list/ListFragmentNavigation;", "navigator", "Lpl/rutka/navigation/Navigator;", "provideMainFragmentNavigation", "Lpl/rutka/main/MainFragmentNavigation;", "navigation_debug"})
@dagger.Module()
public abstract class NavigationModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Binds()
    public abstract pl.rutka.main.MainFragmentNavigation provideMainFragmentNavigation(@org.jetbrains.annotations.NotNull()
    pl.rutka.navigation.Navigator navigator);
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Binds()
    public abstract pl.rutka.list.ListFragmentNavigation provideListFragmentNavigation(@org.jetbrains.annotations.NotNull()
    pl.rutka.navigation.Navigator navigator);
    
    public NavigationModule() {
        super();
    }
}