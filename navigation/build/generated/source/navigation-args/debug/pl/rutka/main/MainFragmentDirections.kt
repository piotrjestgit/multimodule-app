package pl.rutka.main

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import pl.rutka.navigation.R

class MainFragmentDirections private constructor() {
  companion object {
    fun showListFragment(): NavDirections = ActionOnlyNavDirections(R.id.showListFragment)
  }
}
