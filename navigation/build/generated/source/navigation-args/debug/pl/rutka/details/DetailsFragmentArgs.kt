package pl.rutka.details

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavArgs
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import kotlin.Suppress
import kotlin.jvm.JvmStatic
import pl.rutka.model.CatFact

data class DetailsFragmentArgs(
  val catFact: CatFact
) : NavArgs {
  @Suppress("CAST_NEVER_SUCCEEDS")
  fun toBundle(): Bundle {
    val result = Bundle()
    if (Parcelable::class.java.isAssignableFrom(CatFact::class.java)) {
      result.putParcelable("catFact", this.catFact as Parcelable)
    } else if (Serializable::class.java.isAssignableFrom(CatFact::class.java)) {
      result.putSerializable("catFact", this.catFact as Serializable)
    } else {
      throw UnsupportedOperationException(CatFact::class.java.name +
          " must implement Parcelable or Serializable or must be an Enum.")
    }
    return result
  }

  companion object {
    @JvmStatic
    fun fromBundle(bundle: Bundle): DetailsFragmentArgs {
      bundle.setClassLoader(DetailsFragmentArgs::class.java.classLoader)
      val __catFact : CatFact?
      if (bundle.containsKey("catFact")) {
        if (Parcelable::class.java.isAssignableFrom(CatFact::class.java) ||
            Serializable::class.java.isAssignableFrom(CatFact::class.java)) {
          __catFact = bundle.get("catFact") as CatFact?
        } else {
          throw UnsupportedOperationException(CatFact::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        if (__catFact == null) {
          throw IllegalArgumentException("Argument \"catFact\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"catFact\" is missing and does not have an android:defaultValue")
      }
      return DetailsFragmentArgs(__catFact)
    }
  }
}
