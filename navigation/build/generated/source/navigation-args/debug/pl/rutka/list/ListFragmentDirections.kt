package pl.rutka.list

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import pl.rutka.navigation.R

class ListFragmentDirections private constructor() {
  companion object {
    fun showDetailsFragment(): NavDirections = ActionOnlyNavDirections(R.id.showDetailsFragment)
  }
}
