package pl.rutka.navigation

import android.os.Bundle
import androidx.navigation.NavController
import dagger.Binds
import dagger.Module
import dagger.Provides
import pl.rutka.base.Constants.CAT_FACT_KEY
import pl.rutka.list.ListFragmentDirections
import pl.rutka.list.ListFragmentNavigation
import pl.rutka.main.MainFragmentDirections
import pl.rutka.main.MainFragmentNavigation
import pl.rutka.model.CatFact
import javax.inject.Inject
import javax.inject.Singleton

@Module
class NavigatorModule {
    @Singleton
    @Provides
    fun provideNavigator(): Navigator = Navigator()
}

@Module
abstract class NavigationModule {
    @Binds
    abstract fun provideMainFragmentNavigation(navigator: Navigator): MainFragmentNavigation

    @Binds
    abstract fun provideListFragmentNavigation(navigator: Navigator): ListFragmentNavigation
}

class Navigator @Inject constructor() : MainFragmentNavigation, ListFragmentNavigation {

    private var navController: NavController? = null

    fun bind(navController: NavController) {
        this.navController = navController
    }

    fun unbind() {
        navController = null
    }

    override fun openListScreen() {
        navController?.navigate(MainFragmentDirections.showListFragment())
    }

    override fun openCatFactDetails(catFact: CatFact) {
        navController?.navigate(
            ListFragmentDirections.showDetailsFragment().actionId,
            Bundle().apply {
                putSerializable(CAT_FACT_KEY, catFact)
            })

        //FIXME Why I cannot use below ?
//        navController?.navigate(ListFragmentDirections.showDetailsFragment(catFact))
    }
}