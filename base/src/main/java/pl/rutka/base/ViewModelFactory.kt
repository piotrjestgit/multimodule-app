package pl.rutka.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Lazy
import javax.inject.Inject


class ViewModelFactory<VM : ViewModel> @Inject constructor(
    private val viewModel: Lazy<VM>
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return viewModel.get() as T
    }
}

inline fun <reified T : ViewModel> ViewModelFactory<T>.create() = create(T::class.java)

//FIXME ?
inline fun <reified T : ViewModel> ViewModelFactory<T>.getViewModel(fragment: BaseFragment): T {
    return ViewModelProvider(fragment.viewModelStore, this).getAndSetupViewModel(T::class.java)
}

fun <T : ViewModel> ViewModelProvider.getAndSetupViewModel(
    targetViewModelClass: Class<T>
): T {
    return this[targetViewModelClass]
}
