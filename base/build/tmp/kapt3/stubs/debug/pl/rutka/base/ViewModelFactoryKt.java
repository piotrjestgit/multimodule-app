package pl.rutka.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000&\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a$\u0010\u0000\u001a\u0002H\u0001\"\n\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u0003H\u0086\b\u00a2\u0006\u0002\u0010\u0004\u001a\'\u0010\u0005\u001a\u0002H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u0002*\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00010\b\u00a2\u0006\u0002\u0010\t\u001a,\u0010\n\u001a\u0002H\u0001\"\n\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u00032\u0006\u0010\u000b\u001a\u00020\fH\u0086\b\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"create", "T", "Landroidx/lifecycle/ViewModel;", "Lpl/rutka/base/ViewModelFactory;", "(Lpl/rutka/base/ViewModelFactory;)Landroidx/lifecycle/ViewModel;", "getAndSetupViewModel", "Landroidx/lifecycle/ViewModelProvider;", "targetViewModelClass", "Ljava/lang/Class;", "(Landroidx/lifecycle/ViewModelProvider;Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "getViewModel", "fragment", "Lpl/rutka/base/BaseFragment;", "(Lpl/rutka/base/ViewModelFactory;Lpl/rutka/base/BaseFragment;)Landroidx/lifecycle/ViewModel;", "base_debug"})
public final class ViewModelFactoryKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends androidx.lifecycle.ViewModel>T getAndSetupViewModel(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider $this$getAndSetupViewModel, @org.jetbrains.annotations.NotNull()
    java.lang.Class<T> targetViewModelClass) {
        return null;
    }
}