package pl.rutka.api

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Deferred
import pl.rutka.model.CatFact
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun api(retrofit: Retrofit): CatFactsApi = retrofit.create()
}

const val catFactsUrlPath = "/facts"

interface CatFactsApi {
    @GET(catFactsUrlPath)
    fun getCatFactsAsync(): Deferred<CatFactsResponse>
}

data class CatFactsResponse(
    val all: List<CatFact>
)
