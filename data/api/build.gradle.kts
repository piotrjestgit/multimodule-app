
plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
}

android {
    compileSdkVersion(BuildVariables.compileSdkVersion)

    defaultConfig {
        minSdkVersion(BuildVariables.minSdkVersion)
        targetSdkVersion(BuildVariables.targetSdkVersion)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"))
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":data:model"))

    //Dagger
    implementation("com.google.dagger:dagger:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-compiler:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android-support:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-android-processor:${DependencyVersions.dagger}")

    //OkHttp
    implementation("com.squareup.okhttp3:okhttp:${DependencyVersions.okHttp}")
    implementation("com.squareup.okhttp3:logging-interceptor:${DependencyVersions.okHttp}")

    //Retrofit
    implementation("com.squareup.retrofit2:retrofit:${DependencyVersions.retrofit}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${DependencyVersions.retrofitKotlinSerializationConverter}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${DependencyVersions.retrofitCoroutinesAdapter}")

    //Gson
    implementation("com.squareup.retrofit2:converter-gson:${DependencyVersions.gson}")

}