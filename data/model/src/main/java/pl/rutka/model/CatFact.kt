package pl.rutka.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class CatFact(
    val _id: String,
    val text: String
): Parcelable, Serializable