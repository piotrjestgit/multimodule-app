package pl.rutka.repository

import dagger.Module
import dagger.Provides
import pl.rutka.api.CatFactsApi
import pl.rutka.model.CatFact
import pl.rutka.repository.db.SomeDao
import pl.rutka.repository.db.SomeDaoDaoProviderModule
import pl.rutka.repository.db.SomeEntity
import javax.inject.Inject


@Module(includes = [SomeDaoDaoProviderModule::class])
class RepositoryModule {
    @Provides
    fun repository(dao: SomeDao, api: CatFactsApi): Repository = RepositoryImpl(dao, api)
}

interface Repository {
    suspend fun getAllEntities(): List<SomeEntity>?
    suspend fun getCatFacts(): List<CatFact>
}

class RepositoryImpl @Inject constructor(
    private val dao: SomeDao,
    private val api: CatFactsApi
) : Repository {

    override suspend fun getAllEntities(): List<SomeEntity>? {
        return dao.getAll()
    }

    override suspend fun getCatFacts(): List<CatFact> {
        return api.getCatFactsAsync().await().all
    }


}