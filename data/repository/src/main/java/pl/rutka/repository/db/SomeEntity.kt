package pl.rutka.repository.db

import androidx.room.Entity
import androidx.room.PrimaryKey

//data class Something(
//    val id: String,
//    val name: String
//)

@Entity
data class SomeEntity(
    @PrimaryKey
    val id: String,
    val name: String
) {
//    companion object {
//        fun from(some: Something): SomeEntity {
//            return SomeEntity(some.id, some.name)
//        }
//    }
//
//    fun toSome(): Something {
//        return Something(id, name)
//    }
}