package pl.rutka.repository.db

import androidx.room.Dao
import androidx.room.Query
import dagger.Module
import dagger.Provides


@Module
class SomeDaoDaoProviderModule {
    @Provides
    internal fun someDao(appDatabase: AppDatabase): SomeDao = appDatabase.someDao()
}

internal interface SomeDatabase {
    fun someDao(): SomeDaoImpl
}

interface SomeDao {
    suspend fun getAll(): List<SomeEntity>?
}

@Dao
internal interface SomeDaoImpl: SomeDao {

    @Query("select * from SomeEntity")
    override suspend fun getAll(): List<SomeEntity>?

}

