package pl.rutka.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Database(
    entities = [
        SomeEntity::class
    ]
    , version = 1, exportSchema = true
)

internal abstract class AppDatabase : RoomDatabase(), SomeDatabase

@Module
class DatabaseModule {
    @Provides
    @Singleton
    internal fun database(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "database.sqlite").build()
    }
}