
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs.kotlin")
//    id("kotlinx-serialization")
}

android {
    compileSdkVersion(BuildVariables.compileSdkVersion)

    defaultConfig {
        applicationId = "pl.rutka.multimoduleapp"
        minSdkVersion(BuildVariables.minSdkVersion)
        targetSdkVersion(BuildVariables.targetSdkVersion)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"))
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":base"))
    //implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(project(":navigation"))
    implementation(project(":screens:main"))
    implementation(project(":screens:list"))
    implementation(project(":screens:details"))
    implementation(project(":data:repository"))
    implementation(project(":data:api"))
    implementation(project(":data:model"))

    //Multidex
    implementation("androidx.multidex:multidex:${DependencyVersions.multidex}")

    //Dagger
    implementation("com.google.dagger:dagger:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-compiler:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android:${DependencyVersions.dagger}")
    implementation("com.google.dagger:dagger-android-support:${DependencyVersions.dagger}")
    kapt("com.google.dagger:dagger-android-processor:${DependencyVersions.dagger}")

    implementation("org.jetbrains.kotlin:kotlin-stdlib:${DependencyVersions.kotlin_version}")
    implementation("androidx.core:core-ktx:${DependencyVersions.kotlin_core}")
    implementation("androidx.appcompat:appcompat:${DependencyVersions.appCompat}")
    implementation("androidx.constraintlayout:constraintlayout:${DependencyVersions.constraintLayout}")

    //Navigation
    implementation("androidx.navigation:navigation-fragment-ktx:${DependencyVersions.navigation}")
    implementation("androidx.navigation:navigation-ui-ktx:${DependencyVersions.navigation}")

    //OkHttp
    //TODO wykorzystac api()
    implementation("com.squareup.okhttp3:okhttp:${DependencyVersions.okHttp}")
    implementation("com.squareup.okhttp3:logging-interceptor:${DependencyVersions.okHttp}")

    //Retrofit
    implementation("com.squareup.retrofit2:retrofit:${DependencyVersions.retrofit}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${DependencyVersions.retrofitKotlinSerializationConverter}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${DependencyVersions.retrofitCoroutinesAdapter}")

    //Serialization
//    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${DependencyVersions.kotlin_serialization_core}")

    //Gson
    implementation("com.squareup.retrofit2:converter-gson:${DependencyVersions.gson}")

    //Room
    implementation("androidx.room:room-runtime:${DependencyVersions.room}")
    kapt("androidx.room:room-compiler:${DependencyVersions.room}")
    implementation("androidx.room:room-ktx:${DependencyVersions.room}")

    implementation("androidx.fragment:fragment-ktx:${DependencyVersions.fragment_ktx}")

    //Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${DependencyVersions.coroutines}")

    //RecyclerView
    implementation("androidx.recyclerview:recyclerview:${DependencyVersions.recyclerView}")
}