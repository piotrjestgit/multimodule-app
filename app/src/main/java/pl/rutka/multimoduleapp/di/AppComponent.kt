package pl.rutka.multimoduleapp.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import pl.rutka.api.ApiModule
import pl.rutka.api.HttpModule
import pl.rutka.multimoduleapp.App
import pl.rutka.navigation.NavigationModule
import pl.rutka.navigation.NavigatorModule
import pl.rutka.repository.RepositoryModule
import pl.rutka.repository.db.DatabaseModule
import pl.rutka.repository.db.SomeDaoDaoProviderModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidInjectorsModule::class,
        HttpModule::class,
        ApiModule::class,
        NavigatorModule::class,
        NavigationModule::class,
        RepositoryModule::class,
        DatabaseModule::class,
        SomeDaoDaoProviderModule::class
    ]
)

interface AppComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<App>
}
