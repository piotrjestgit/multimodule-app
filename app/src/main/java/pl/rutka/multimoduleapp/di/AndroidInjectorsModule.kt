package pl.rutka.multimoduleapp.di

import android.content.Context
import dagger.Binds
import dagger.Module
import pl.rutka.details.DetailsFragmentModule
import pl.rutka.list.ListFragmentModule
import pl.rutka.main.MainFragmentModule
import pl.rutka.multimoduleapp.App
import pl.rutka.multimoduleapp.MainActivityModule

@Module(
    includes = [
        MainActivityModule::class,
        MainFragmentModule::class,
        ListFragmentModule::class,
        DetailsFragmentModule::class
    ]
)

interface AndroidInjectorsModule {
    @Binds
    fun context(app: App): Context
}
