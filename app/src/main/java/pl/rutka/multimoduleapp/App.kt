package pl.rutka.multimoduleapp

import android.app.Activity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import pl.rutka.multimoduleapp.di.DaggerAppComponent
import javax.inject.Inject

class App : androidx.multidex.MultiDexApplication(),
    HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        super.onCreate()

        val component = DaggerAppComponent.factory().create(this)
        component.inject(this)

    }

}