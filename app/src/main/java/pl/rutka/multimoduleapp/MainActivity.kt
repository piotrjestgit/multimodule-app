package pl.rutka.multimoduleapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import dagger.Module
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import pl.rutka.navigation.Navigator
import javax.inject.Inject

@Module
interface MainActivityModule {
    @ContributesAndroidInjector
    fun mainActivity(): MainActivity
}

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = supportFragmentInjector

    private val navigationController by lazy { findNavController(R.id.nav_host_fragment) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)

        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        navigator.bind(navigationController)
    }

    override fun onPause() {
        super.onPause()
        navigator.unbind()
    }
}