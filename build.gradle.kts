
buildscript {

    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:${DependencyVersions.gradle}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${DependencyVersions.kotlin_version}")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:${DependencyVersions.navigation}")
//        classpath(kotlin("serialization", version = DependencyVersions.kotlin_version))
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}